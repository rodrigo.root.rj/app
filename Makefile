VERSION:=$(shell cat version)
REPO=rodrigorootrj
PROJECT=webapp
TAG=${REPO}/${PROJECT}:${VERSION}


build:
	docker build . -t ${TAG}
push: build
	docker push ${TAG}
run: build
	docker run -it ${TAG}
test: 
	docker run -itd ${TAG}