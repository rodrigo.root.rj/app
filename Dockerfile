FROM python:3.8
RUN mkdir /app
WORKDIR /app
COPY . /app
CMD ["python","run.py"]